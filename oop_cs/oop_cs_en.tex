%&tex

\documentclass{beamer}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage[singlelinecheck=false]{caption}
\usepackage{subcaption}
\usepackage{minted}
\usepackage{xcolor}

\uselanguage{English}
\languagepath{English}

\setbeamertemplate{theorems}[numbered]

\usemintedstyle[c]{perldoc}

\newcommand{\code}[1]{\lstinline[mathescape=true]{#1}}
\newcommand{\mcode}[1]{\lstinline[mathescape]!#1!}

\newcommand{\algorithmautorefname}{Algorithm}
\algrenewcommand\algorithmicrequire{\textbf{Entrada}}
\algrenewcommand\algorithmicensure{\textbf{Saída}}
\algrenewcommand\algorithmicif{\textbf{se}}
\algrenewcommand\algorithmicthen{\textbf{então}}
\algrenewcommand\algorithmicelse{\textbf{senão}}
\algrenewcommand\algorithmicfor{\textbf{para todo}}
\algrenewcommand\algorithmicdo{\textbf{faça}}
\algnewcommand{\LineComment}[1]{\State\,\(\triangleright\) #1}

\newcommand{\defeq}{\vcentcolon=}

\title{\huge Object Oriented Programming in C:\\ A Case Study}
\subtitle{Git and Kernel Linux}
\date{}
\author{Matheus Tavares \texttt{<matheus.bernardino@usp.br>}\\
Renato Lui Geh \texttt{<renatolg@ime.usp.br>}\\~\\
\footnotesize\textcolor{gray}{Institute of Mathematics and Statistics - University of São Paulo}}

% If \titlelogo is not set, macaw does not add a title logo to the title page.
\newcommand{\titlelogo}{imgs/logo.png}

\usetheme{macaw}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{\LARGE OO Techniques in non-OO Languages}

  \textbf{Motivation:} Combine good features of a language with good but absent
  techniques from other languages.
  \vfill

  \begin{quote}
    No programming technique solves all problems.\newline
    No programming language produces only correct results.
    \vskip5mm
    \hspace*\fill{\small--- Schreiner, Axel T (1993). Object-Oriented Programming With ANSI-C }
  \end{quote}
\end{frame}

\begin{frame}
  \frametitle{Content}

  We'll see the following concepts implemented in C:\\

  \begin{itemize}
    \item Private attributes
    \item Private methods
    \item ``Public'' Inheritance
    \item ``Private'' Inheritance
    \item Multiple inheritance
    \item Abstract Classes and Polymorphism
    \item Metaprogramming
    \item Design Pattern: Iterator
  \end{itemize}\vspace{0.5cm}

  In particular, we'll talk about implementation examples of these concepts in
  the \textbf{Git} and \textbf{Kernel Linux IIO} codebases.
\end{frame}

\section{Kernel Linux IIO}

\begin{frame}
  \frametitle{Linux IIO}

  \small
  \begin{itemize}
    \item \mintinline{c}{iio_dev}: Industrial Input/Output Device
    \item \mintinline{c}{ad7780_state}: Analog to Digital Sigma-Delta Converter Device
  \end{itemize}\vspace{0.5cm}

  \begin{figure}
    \centering
    \tikzset{every picture/.style={line width=0.75pt}}
    \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
      \draw   (110.25,139.72) -- (210.25,139.72) -- (210.25,210.5) -- (110.25,210.5) -- cycle ;
      \draw   (120.25,168.45) .. controls (120.25,163.92) and (123.92,160.25) .. (128.45,160.25) -- (192.05,160.25) .. controls (196.58,160.25) and (200.25,163.92) .. (200.25,168.45) -- (200.25,193.05) .. controls (200.25,197.58) and (196.58,201.25) .. (192.05,201.25) -- (128.45,201.25) .. controls (123.92,201.25) and (120.25,197.58) .. (120.25,193.05) -- cycle ;
      \draw (160.89,147.44) node [scale=0.8] [align=left] {{\fontfamily{pcr}\selectfont iio\_dev}};
      \draw (160.5,170) node [scale=0.7] [align=left] {{\fontfamily{pcr}\selectfont ad7780\_state}};
    \end{tikzpicture}
  \end{figure}\vspace{0.5cm}

  \begin{itemize}
    \item \mintinline{c}{struct ad7780_state} specialization of \mintinline{c}{struct iio_dev}.
    \item In other words, \mintinline{c}{ad7780_state} is a \textbf{subclass} of
      \mintinline{c}{iio_dev}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Inheritance}

  \textbf{Inheritance by composition}
  \vfill

  Let $S$ be the superclass, and $C$ a subclass of $S$. Assume $n$ and $m$, $S$ and $C$'s memory
  size in bytes. We create an object of $C$ in the following way:\\

  \begin{enumerate}
    \item Allocate a block $B$ of size $n+m+a$ (in bytes);
    \item Save first $n$ bytes $[0, n]$ for $S$;
    \item Save last $m$ bytes $[n+a,n+a+m]$ for $C$;
    \item Return a pointer to block $B$ of type $*S$.
  \end{enumerate}~\\

  $C$ is now a ``private'' object of $S$.
\end{frame}

\begin{frame}
  \frametitle{Inside the Kernel}

  \begin{block}{Definitions}
    \vspace{-0.5cm}
    \begin{flalign*}
      \quad S &\defeq \text{\mintinline{c}{iio_dev}}&&\\
      C &\defeq \text{\mintinline{c}{ad7780_state}}\\
      n &\defeq \text{\mintinline{c}{sizeof(struct iio_dev)}}\\
      m &\defeq \text{\mintinline{c}{sizeof(struct ad7780_state)}}
    \end{flalign*}
  \end{block}\vfill

  Function \mintinline{c}{devm_iio_device_alloc} allocs block $B$ and returns a pointer
  \mintinline{c}{struct iio_dev*} to the beginning of the block.\vfill

  \begin{itemize}
    \small
    \item \mintinline{c}{drivers/iio/adc/ad7780.c:ad7780_state}
    \item \mintinline{c}{include/linux/iio/iio.h:iio_dev}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Access to the subclass}

  How to access $c$ from an $s$ pointer given the address of a block $B$?
  \vfill

  \begin{minted}{c}
#define ALIGN(x, a) ALIGN_MASK(x, (typeof(x))(a)-1)
#define ALIGN_MASK(x, mask) (((x) + (mask)) & ~(mask))
#define ALIGN_BYTES CACHE_BYTES_IN_POWER_OF_2

static inline void *priv(const struct S *s) {
  /* Returns a pointer to c "hidden" in s. */
  return (char*) s + ALIGN(sizeof(struct S), ALIGN_BYTES);
}
  \end{minted}
  \vfill

  \begin{itemize}
    \item \mintinline{c}{include/linux/kernel.h:ALIGN}
    \item \mintinline{c}{include/linux/iio/iio.h:iio_priv}
    \item \mintinline{c}{include/uapi/linux/kernel.h:__ALIGN_KERNEL_MASK}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Understanding \mintinline{c}{ALIGN}}

  \mintinline{c}{ALIGN} is a function parameterized by the size of $S$ and some power of two.\\

  \begin{block}{Recall from CS101...}
    Access to memory is faster when address is a power of two.
  \end{block}\vspace{0.5cm}

  We want to access an address of the alloc'ed memory somewhere near \mintinline{c}{s+sizeof(S)},
  and it must be a power of two.\\\textcolor{gray}{(see next slide)}\vspace{0.5cm}

  \begin{alertblock}{Claim}
    \mintinline{c}{ALIGN(x, a)} is \textbf{the smallest multiple of \mintinline{c}{a}
    greater than \mintinline{c}{x}}.
  \end{alertblock}
\end{frame}

\begin{frame}

  \begin{center}
    \mintinline{c}{ad7780_state* = priv(iio_dev*) = iio_dev* + ALIGN(...)}
  \end{center}
  \vspace{0.125cm}
  \begin{figure}
    \centering
    \tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt

    \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
    \draw   (100,121) -- (309,121) -- (309,381.14) -- (100,381.14) -- cycle ;
    \draw   (314,250.14) .. controls (318.67,250.14) and (321,247.81) .. (321,243.14) -- (321,195.64) .. controls (321,188.97) and (323.33,185.64) .. (328,185.64) .. controls (323.33,185.64) and (321,182.31) .. (321,175.64)(321,178.64) -- (321,128.14) .. controls (321,123.47) and (318.67,121.14) .. (314,121.14) ;
    \draw   (313,280.14) .. controls (316.98,280.28) and (319.04,278.36) .. (319.18,274.38) -- (319.18,274.38) .. controls (319.37,268.69) and (321.46,265.92) .. (325.44,266.05) .. controls (321.46,265.92) and (319.57,263.01) .. (319.76,257.32)(319.67,259.88) -- (319.76,257.32) .. controls (319.9,253.34) and (317.98,251.28) .. (314,251.14) ;
    \draw   (313,379.14) .. controls (317.67,379.19) and (320.02,376.88) .. (320.07,372.21) -- (320.4,339.71) .. controls (320.47,333.04) and (322.83,329.73) .. (327.5,329.78) .. controls (322.83,329.73) and (320.53,326.38) .. (320.6,319.71)(320.57,322.71) -- (320.93,287.21) .. controls (320.98,282.54) and (318.67,280.19) .. (314,280.14) ;
    \draw    (316,280.14) -- (353,280.14) ;
    \draw [shift={(355,280.14)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
    \draw   (110,280.14) -- (300,280.14) -- (300,369.14) -- (110,369.14) -- cycle ;
    \draw   (110,130.14) -- (300,130.14) -- (300,250.14) -- (110,250.14) -- cycle ;
    \draw (339,185.14) node  [align=left] {$\displaystyle n$};
    \draw (338,328.14) node  [align=left] {$\displaystyle m$};
    \draw (336,265.14) node  [align=left] {$\displaystyle a$};
    \draw (394,278.14) node  [align=left] {$\displaystyle n+a\ \mid\ 2^{c+p}$};
    \draw (205,292.14) node  [align=left] {ad7780\_state};
    \draw (202,141.7) node  [align=left] {iio\_dev};
    \end{tikzpicture}
    \caption{``Private'' inheritance. Given \mintinline{c}{iio_dev*}, how do we find
      \mintinline{c}{ad7780_state*} in a fast, memory efficient way?}
  \end{figure}
\end{frame}

\begin{frame}
  \begin{lemma}
    Let $n,m\in\mathbb{Z}_k$, st $m=2^c-1$ for some $c\in\mathbb{N}, c<k$. Then

    \begin{equation*}
      (n+m) \text{ \& } \sim(m) \mid 2^{c+p}\text{, for some $p\in\mathbb{N}$.}
    \end{equation*}

    \label{multiple}
  \end{lemma}

  \textbf{Proof.}\\

  First note that $m=2^c-1=(0\ldots 01\ldots 1)_2$, so $(\sim m)=(1\ldots 10\ldots 0)_2$.
  Therefore $(n+m)\text{ \& }\sim(m)$ are the most significant bits of $n+2^c-1$ starting from the
  $c$-th bit. But that's exactly taking $n+m$ and ``subtracting'' all the least significant bits
  starting from $c-1$. More formally,

  \begin{equation*}
    (n+m)\text{ \& }\sim(m)=(n+m)-((n+m)\text{ \& }(2^c-1)).
  \end{equation*}

\end{frame}

\begin{frame}
  The right-hand term on the right side of the equality can be rewritten as

  \begin{equation*}
    ((n+m)\text{ \& }(2^c-1)) \equiv (n+m)\mod 2^c.
  \end{equation*}

  In other words, taking the $c-1$ least significant bits is equivalent to taking the remainder of
  the division by $2^c$. To give some intuition, note that the $2^i$ bits for all $i>c$ are all
  multiples of $2^c$, and therefore equivalent to zero. From that
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+m)-\left((n+m)\mod 2^c\right)\\
                            &= (n+2^c-1)-((n+2^c-1)\mod 2^c)\\
                            &= (n+2^c-1)-((n-1)\mod 2^c).
  \end{align*}

  If $n<2^c$, then $(n+2^c-1)-n+1=2^c\mid 2^c$ and therefore the hypothesis is trivially true. The
  same can be said when $n=2^c$. Now, assuming $n>2^c$, let's analize the two possible cases for
  $n$.
\end{frame}

\begin{frame}
  \textbf{Case 1:} $2^c\mid n$
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= (n+2^c-1)+1\\
                            &= (n+2^c)\mid 2^{c+p}.\text{\color{gray}\quad(by assumption)}
  \end{align*}

  \textbf{Case 2:} $2^c\nmid n$
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= (n-r+r+2^c-1)-((r-1)\mod 2^c),
  \end{align*}~\\

  where $r=n\mod 2^c$, that is, the ``remainder'' of $n/2^c$.
\end{frame}

\begin{frame}
  We get two subcases: when $0\equiv r-1\mod 2^c$, and thus
  \vspace{0.75cm}
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((r-1)\mod 2^c)\\
                            &= (n-r+r+2^c-1)\\
                            &= ((n-r)+(r-1)+2^c)\mid 2^{c+p},
  \end{align*}
  \vspace{0.3cm}

  Note that $n-r=2^c$, since $r=n\mod 2^c$; and $r-1=0$, as $r\equiv 1\mod 2^c$ and $r<2^c$ by
  definition. This means $(n-r)+(r-1)=2^c$, and thus $(2^c+2^c)=2^{c+1}\mid 2^{c+p}$.
\end{frame}

\begin{frame}
  Finally, we analyze the last case: $0\not\equiv r-1\mod 2^c$. In this subcase, since $r<2^c$,
  $(r-1\mod 2^c)=r-1$ so:
  \vspace{0.7cm}
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((r-1)\mod 2^c)\\
                            &= (n-r+r+2^c-1)-r+1\\
                            &= ((n-r)+2^c)\mid 2^{c+p}.
  \end{align*}
  \vspace{0.3cm}

  And therefore $(n+m)\text{ \& }\sim(m)\mid 2^{c+p}$, for some integer $p$.
  \qed
\end{frame}

\begin{frame}
  \begin{lemma}
    Let $n,m\in\mathbb{Z}_k$, st $m=2^c-1$ for some $c\in\mathbb{N}, c<k$. Then

    \begin{equation*}
      t=(n+m) \text{ \& } \sim(m)
    \end{equation*}

    Is the smallest multiple of $2^c$ greater than $n$.\label{minimum}
  \end{lemma}

  \textbf{Proof.}

  We will show by exaustion that $t$ is in fact the minimum candidate multiple of $2^c$ with
  respect to $n$. Recall the cases shown in Lemma \ref{multiple}'s proof. When $n< 2^c$,
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= n+2^c-1-n+1\\
                            &= 2^c = t
  \end{align*}
  and therefore $t$ is the smallest multiple of $2^c$ greater than $n$.
\end{frame}

\begin{frame}
  For $n=2^c$,
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= 2^c+2^c-1+1\\
                            &= 2^c+2^c = t,
  \end{align*}
  again $t$ is the ``minimum'' multiple. Recall the two cases when $n>2^c$.\\

  \textbf{Case 1:} $2^c\mid n$
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= (n+2^c-1)+1\\
                            &= n+2^c = t
  \end{align*}
  If $n$ is a multiple of $2^c$, then the next multiple greater than $n$ is $n+2^c$.
\end{frame}

\begin{frame}
  \textbf{Case 2:} $2^c\nmid n$\\

  When $0\equiv r-1\mod 2^c$, $r=1$, since $r<2^c$ by definition.
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((r-1)\mod 2^c)\\
                            &= n-1+2^c = t
  \end{align*}
  But if $r=1$, then $n-1$ is a multiple of $2^c$, and therefore the smallest multiple greater than
  $n$, which is exactly $n-1+2^c$.\\

  For the last subcase, take $0\not\equiv r-1\mod 2^c$. Then
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((r-1)\mod 2^c)\\
                            &= (n-r+r+2^c-1)-r+1\\
                            &= n-r+2^c. = t
  \end{align*}
  Again, $n-r$ is a multiple of $2^c$ by definition, and therefore the next candidate is $n-r+2^c$.
  \qed
\end{frame}

\begin{frame}
  \begin{alertblock}{Claim}
    \mintinline{c}{ALIGN(x, a)} is \textbf{the smallest multiple of \mintinline{c}{a}
    greater than \mintinline{c}{x}}.
  \end{alertblock}

  \vspace{0.30cm}
  Our claim, but fancier:
  \vspace{0.30cm}

  \begin{theorem}
    The function \textnormal{\mintinline{c}{ALIGN(sizeof(struct S), ALIGN_BYTES)}} returns the
    smallest address of memory multiple of \textnormal{\mintinline{c}{ALIGN_BYTES}} greater than
    \textnormal{\mintinline{c}{sizeof(struct S)}}.
  \end{theorem}
  \vfill

  \textbf{Proof.}\\

  Follows directly from Lemma \ref{multiple} and Lemma \ref{minimum}.

  \qed
\end{frame}

\begin{frame}[fragile]
  \frametitle{\mintinline{c}{ALIGN} in the wild}

  \begin{minted}{c}
static int ad7780_probe(struct spi_device *spi) {
  struct ad7780_state *st;
  struct iio_dev *indio_dev;

  indio_dev = devm_iio_device_alloc(&spi->dev, sizeof(*st));
  if (!indio_dev) return -ENOMEM;

  st = iio_priv(indio_dev);
  st->gain = 1;
  ...
}
  \end{minted}

  \vspace{0.5cm}
  \begin{itemize}
    \item \mintinline{c}{drivers/iio/adc/ad7780.c:ad7780_probe}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Multiple inheritance}

  \small
  \begin{itemize}
    \item \mintinline{c}{ad7780_state} child of \mintinline{c}{iio_dev} (\textbf{``private''
      inheritance});
    \item \mintinline{c}{ad7780_state} child of \mintinline{c}{ad_sigma_delta}
      (\textbf{``public'' inheritance}).
  \end{itemize}
  \normalsize
  \vfill

  \begin{figure}
    \centering
    \tikzset{every picture/.style={line width=0.75pt}}
    \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
      \draw   (110.25,139.72) -- (210.25,139.72) -- (210.25,210.5) -- (110.25,210.5) -- cycle ;
      \draw    (160.25,240.25) -- (160.25,192.75) ;
      \draw [shift={(160.25,190.75)}, rotate = 450] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
      \draw   (110.75,241) -- (209.75,241) -- (209.75,319.75) -- (110.75,319.75) -- cycle ;
      \draw   (119.5,278.45) .. controls (119.5,273.92) and (123.17,270.25) .. (127.7,270.25) -- (192.05,270.25) .. controls (196.58,270.25) and (200.25,273.92) .. (200.25,278.45) -- (200.25,303.05) .. controls (200.25,307.58) and (196.58,311.25) .. (192.05,311.25) -- (127.7,311.25) .. controls (123.17,311.25) and (119.5,307.58) .. (119.5,303.05) -- cycle ;
      \draw   (120.25,168.45) .. controls (120.25,163.92) and (123.92,160.25) .. (128.45,160.25) -- (192.05,160.25) .. controls (196.58,160.25) and (200.25,163.92) .. (200.25,168.45) -- (200.25,193.05) .. controls (200.25,197.58) and (196.58,201.25) .. (192.05,201.25) -- (128.45,201.25) .. controls (123.92,201.25) and (120.25,197.58) .. (120.25,193.05) -- cycle ;
      \draw (160.89,147.44) node [scale=0.8] [align=left] {{\fontfamily{pcr}\selectfont iio\_dev}};
      \draw (160.5,251) node [scale=0.7] [align=left] {{\fontfamily{pcr}\selectfont ad7780\_state}};
      \draw (160.25,280.38) node  [align=left] {{\fontfamily{pcr}\selectfont {\tiny ad\_sigma\_delta}}};
    \end{tikzpicture}
  \end{figure}
  \vfill

  Both use inheritance by composition, but in different ways.
\end{frame}

\begin{frame}
  \frametitle{``Public'' vs ``private'' inheritance}
  \small

  \begin{block}{Private inheritance}
    \small
    As seen on \mintinline{c}{iio_dev} and \mintinline{c}{ad7780_state}.
    \begin{itemize}
      \item Subclass attributes are private;
      \item Runtime inheritance;
      \item Subclass could be of any type (\mintinline{c}{ad7780_state},
        \mintinline{c}{ad7793_state}, \mintinline{c}{mcp3422}, etc).
    \end{itemize}
  \end{block}

  We shall now see ``public'' inheritance.

  \begin{block}{Public inheritance}
    To be seen on \mintinline{c}{ad_sigma_delta} and \mintinline{c}{ad7780_state}.
    \begin{itemize}
      \item Attributes of superclass and subclass are public;
      \item Compile-time inheritance;
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \begin{description}
    \item[\mintinline{c}{ad_sigma_delta}:] Analog-Digital Sigma-Delta Converter (ADSD)
    \item[\mintinline{c}{ad7780_state}:] ADSD Converter for AD7170/1 and AD7780/1
  \end{description}\vspace{0.2cm}

  \begin{minted}{c}
struct ad7780_state {
  const struct ad7780_chip_info *chip_info;
  struct regulator *reg;
  struct gpio_desc *powerdown_gpio;
  ...
  unsigned int int_vref_mv;

  struct ad_sigma_delta sd;
}
  \end{minted}

  \vspace{0.1cm}
  In private inheritance, the \textbf{superclass (\mintinline{c}{iio_dev}) contains the subclass
  (\mintinline{c}{ad7780_state})}.\\

  In public inheritance, the \textbf{subclass (\mintinline{c}{ad7780_state}) contains the
  superclass (\mintinline{c}{ad_sigma_delta})}.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Access to the subclass}

  How to access object $c$ of subclass $C$ when object $s$ of type $S$ is \emph{inside} $C$?\\

  \begin{minted}[fontsize=\small]{c}
#define container_of(ptr, type, member) \
  (type*)((void*)(ptr) - ((size_t)&((type*)0)->member)

static struct ad7780_state *ad_sigma_delta_to_ad7780(
  struct ad_sigma_delta *sd) {
    return container_of(sd, struct ad7780_state, sd);
}
  \end{minted}

  \vspace{0.5cm}
  \begin{itemize}
    \small
    \item \mintinline{c}{drivers/iio/adc/ad7780.c:ad_sigma_delta_to_ad7780}
    \item \mintinline{c}{include/linux/kernel.h:container_of}
    \item \mintinline{c}{include/linux/stddef.h:offsetof}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Understanding \mintinline{c}{container_of}}

  We want to access the ``outer'' pointer, that is, find the pointer
  \mintinline{c}{struct ad7780_state*} that contains \mintinline{c}{struct ad_sigma_delta*}.\\

  \begin{minted}[fontsize=\small]{c}
#define container_of(ptr, type, member) \
  (type*)((void*)(ptr) - ((size_t)&((type*)0)->member)

static struct ad7780_state *ad_sigma_delta_to_ad7780(
  struct ad_sigma_delta *sd) {
    return container_of(sd, struct ad7780_state, sd);
}
  \end{minted}

  \vspace{0.3cm}
  \begin{block}{Trick}
    \mintinline{c}{&((type*)0)->member}: returns the address of \mintinline{c}{member} as if
    \mintinline{c}{type*} were allocated to the zero-address. In other words, the size (in bytes)
    of \mintinline{c}{type} \emph{up to} variable \mintinline{c}{member}.
    \textcolor{gray}{(see next slide)}
  \end{block}
\end{frame}

\begin{frame}
  \begin{center}
    \small\mintinline{c}{container_of(p, C, p)=(C*)((void*)p-((size_t)&((C*)0)->p))}
  \end{center}
  \vspace{0.125cm}
  \begin{figure}
    \centering
    \tikzset{every picture/.style={line width=0.75pt}}
    \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
    \draw   (120,141) -- (329,141) -- (329,388.71) -- (120,388.71) -- cycle ;
    \draw   (334,221.29) .. controls (338.67,221.29) and (341,218.96) .. (341,214.29) -- (341,191.21) .. controls (341,184.54) and (343.33,181.21) .. (348,181.21) .. controls (343.33,181.21) and (341,177.88) .. (341,171.21)(341,174.21) -- (341,148.14) .. controls (341,143.47) and (338.67,141.14) .. (334,141.14) ;
    \draw   (334,328.29) .. controls (338.67,328.29) and (341,325.96) .. (341,321.29) -- (341,284.71) .. controls (341,278.04) and (343.33,274.71) .. (348,274.71) .. controls (343.33,274.71) and (341,271.38) .. (341,264.71)(341,267.71) -- (341,228.14) .. controls (341,223.47) and (338.67,221.14) .. (334,221.14) ;
    \draw   (130,221.07) -- (320,221.07) -- (320,330.14) -- (130,330.14) -- cycle ;
    \draw   (334,387.71) .. controls (338.67,387.71) and (341,385.38) .. (341,380.71) -- (341,368.43) .. controls (341,361.76) and (343.33,358.43) .. (348,358.43) .. controls (343.33,358.43) and (341,355.1) .. (341,348.43)(341,351.43) -- (341,336.14) .. controls (341,331.47) and (338.67,329.14) .. (334,329.14) ;
    \draw    (335,221.71) -- (371,221.71) ;
    \draw [shift={(373,221.71)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
    \draw (358,180.14) node  [align=left] {$\displaystyle n_{1}$};
    \draw (362,274.14) node  [align=left] {$\displaystyle m$};
    \draw (226,235.14) node  [align=left] {ad\_sigma\_delta};
    \draw (222,161.7) node  [align=left] {ad7780\_state};
    \draw (358,358.14) node  [align=left] {$\displaystyle n_{2}$};
    \draw (382,222.71) node  [align=left] {$\displaystyle p$};
    \end{tikzpicture}
    \caption{``Public'' inheritance. Given \mintinline{c}{ad_sigma_delta*}, how do we find
    \mintinline{c}{ad7780_state*}?}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Summary}
  \begin{figure}
    \centering
    \tikzset{every picture/.style={line width=0.75pt}}
    \resizebox{0.53\textwidth}{!}{
      \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
      \draw   (100,121) -- (309,121) -- (309,381.14) -- (100,381.14) -- cycle ;
      \draw   (314,250.14) .. controls (318.67,250.14) and (321,247.81) .. (321,243.14) -- (321,195.64) .. controls (321,188.97) and (323.33,185.64) .. (328,185.64) .. controls (323.33,185.64) and (321,182.31) .. (321,175.64)(321,178.64) -- (321,128.14) .. controls (321,123.47) and (318.67,121.14) .. (314,121.14) ;
      \draw   (313,280.14) .. controls (316.98,280.28) and (319.04,278.36) .. (319.18,274.38) -- (319.18,274.38) .. controls (319.37,268.69) and (321.46,265.92) .. (325.44,266.05) .. controls (321.46,265.92) and (319.57,263.01) .. (319.76,257.32)(319.67,259.88) -- (319.76,257.32) .. controls (319.9,253.34) and (317.98,251.28) .. (314,251.14) ;
      \draw   (313,379.14) .. controls (317.67,379.19) and (320.02,376.88) .. (320.07,372.21) -- (320.4,339.71) .. controls (320.47,333.04) and (322.83,329.73) .. (327.5,329.78) .. controls (322.83,329.73) and (320.53,326.38) .. (320.6,319.71)(320.57,322.71) -- (320.93,287.21) .. controls (320.98,282.54) and (318.67,280.19) .. (314,280.14) ;
      \draw    (316,280.14) -- (353,280.14) ;
      \draw [shift={(355,280.14)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
      \draw   (110,280.14) -- (300,280.14) -- (300,369.14) -- (110,369.14) -- cycle ;
      \draw   (110,130.14) -- (300,130.14) -- (300,250.14) -- (110,250.14) -- cycle ;
      \draw (339,185.14) node  [align=left] {$\displaystyle n$};
      \draw (338,328.14) node  [align=left] {$\displaystyle m$};
      \draw (336,265.14) node  [align=left] {$\displaystyle a$};
      \draw (394,278.14) node  [align=left] {$\displaystyle n+a \mid 2^{c+p}$};
      \draw (205,292.14) node  [align=left] {ad7780\_state};
      \draw (202,141.7) node  [align=left] {iio\_dev};
      \end{tikzpicture}
    }
    \tikzset{every picture/.style={line width=0.75pt}}
    \resizebox{0.44\textwidth}{!}{
      \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
      \draw   (120,141) -- (329,141) -- (329,388.71) -- (120,388.71) -- cycle ;
      \draw   (334,221.29) .. controls (338.67,221.29) and (341,218.96) .. (341,214.29) -- (341,191.21) .. controls (341,184.54) and (343.33,181.21) .. (348,181.21) .. controls (343.33,181.21) and (341,177.88) .. (341,171.21)(341,174.21) -- (341,148.14) .. controls (341,143.47) and (338.67,141.14) .. (334,141.14) ;
      \draw   (334,328.29) .. controls (338.67,328.29) and (341,325.96) .. (341,321.29) -- (341,284.71) .. controls (341,278.04) and (343.33,274.71) .. (348,274.71) .. controls (343.33,274.71) and (341,271.38) .. (341,264.71)(341,267.71) -- (341,228.14) .. controls (341,223.47) and (338.67,221.14) .. (334,221.14) ;
      \draw   (130,221.07) -- (320,221.07) -- (320,330.14) -- (130,330.14) -- cycle ;
      \draw   (334,387.71) .. controls (338.67,387.71) and (341,385.38) .. (341,380.71) -- (341,368.43) .. controls (341,361.76) and (343.33,358.43) .. (348,358.43) .. controls (343.33,358.43) and (341,355.1) .. (341,348.43)(341,351.43) -- (341,336.14) .. controls (341,331.47) and (338.67,329.14) .. (334,329.14) ;
      \draw    (335,221.71) -- (371,221.71) ;
      \draw [shift={(373,221.71)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
      \draw (358,180.14) node  [align=left] {$\displaystyle n_{1}$};
      \draw (362,274.14) node  [align=left] {$\displaystyle m$};
      \draw (226,235.14) node  [align=left] {ad\_sigma\_delta};
      \draw (222,161.7) node  [align=left] {ad7780\_state};
      \draw (358,358.14) node  [align=left] {$\displaystyle n_{2}$};
      \draw (382,222.71) node  [align=left] {$\displaystyle p$};
      \end{tikzpicture}
    }
    \vspace{0.25cm}
    \caption{Different approaches for different uses. Which one is better? Depends on what you
    need.}
  \end{figure}
\end{frame}

\section{Git}

\begin{frame}[fragile]
  \frametitle{The \mintinline{c}{dir-iterator} object}

  Usage example (simplified):
  \vfill

  \begin{minted}{c}
struct dir_iterator *iter = dir_iterator_begin(path);

while (dir_iterator_advance(iter) == ITER_OK) {
  if (want_to_stop_iteration()) {
    dir_iterator_abort(iter);
    break;
  }

  // Access information about the current path:
  if (S_ISDIR(iter->st.st_mode))
    printf("%s is a directory\n", iter->relative_path);
}
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{API: public attributes and methods}
  \begin{minted}{c}
// The current iteration state, with path,
// basename and etc.
struct dir_iterator {
  struct strbuf path;
  const char *relative_path;
  const char *basename;
  struct stat st;
};

struct dir_iterator *dir_iterator_begin(const char *path);
int dir_iterator_abort(struct dir_iterator *iterator);
int dir_iterator_advance(struct dir_iterator *iterator);
  \end{minted}

  \vspace{0.5cm}
  \begin{itemize}
    \item \mintinline{c}{dir-iterator.h}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Full declaration and constructor}

  %\begin{minted}[fontsize=\small]{c}
%struct dir_iterator_int {
  %struct dir_iterator base;

  %size_t levels_nr;
  %size_t levels_alloc;
  %struct dir_iterator_level *levels;
  %unsigned int flags;
%};

%struct dir_iterator *dir_iterator_begin(const char *path) {
  %struct dir_iterator_int *iter = xcalloc(1, sizeof(*iter));
  %struct dir_iterator *dir_iterator = &iter->base;
  %... /* Initialize fields. */
  %return dir_iterator;
%}
  %\end{minted}
  \begin{figure}
    \centering\includegraphics[width=1.05\linewidth]{imgs/dir-iterator-initialization.png}
  \end{figure}

  \begin{itemize}
    \item \mintinline{c}{dir-iterator.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{How to access private attributes?}

  \begin{minted}[fontsize=\small]{c}
int dir_iterator_advance(struct dir_iterator *dir_iterator)
{
	struct dir_iterator_int *iter =
		(struct dir_iterator_int *)dir_iterator;
        // Use iter as needed
	...
}
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{dir-iterator.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Private attributes: how it works}
  \begin{figure}
    \centering\includegraphics[width=1\linewidth]{imgs/dir-iterator-memory.png}
  \end{figure}

  \begin{itemize}
    \item Use this technique with caution:
    \begin{itemize}
      \item \mintinline{c}{memcpy} and others:
      {\scriptsize \mintinline{c}{sizeof(struct dir_iterator) != sizeof(struct dir_iterator_int)}}
      \item arrays and initializations out of {\scriptsize \mintinline{c}{dir_iterator_begin()}}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{What about private methods?}

  \begin{minted}[fontsize=\small]{c}
int dir_iterator_advance(struct dir_iterator *dir_iterator)
{
	struct dir_iterator_int *iter =
		(struct dir_iterator_int *)dir_iterator;

	if (/* ... */ && push_level(iter))
	...
}

static int push_level(struct dir_iterator_int *iter)
{
	// Use iter as needed
}
  \end{minted}

  \begin{itemize}
    \item \mintinline{c}{dir-iterator.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Abstract classes, inheritance and polymorphism}


  \begin{itemize}
    \item \mintinline{c}{refs/refs-internal.h}
    \item \mintinline{c}{refs/iterator.c}
    \item \mintinline{c}{refs/files-backend.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\textit{Big picture}}
  \begin{figure}
    \centering\includegraphics[width=1.05\linewidth]{imgs/iterator-uml-en.png}
  \end{figure}
\end{frame}

\begin{frame}[fragile]
  \frametitle{The abstract class \mintinline{c}{ref_iterator}}

  \begin{minted}[fontsize=\small]{c}
struct ref_iterator {
	struct ref_iterator_vtable *vtable;

	unsigned int ordered : 1;
	const char *refname;
	const struct object_id *oid;
	unsigned int flags;
};
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{refs/refs-internal.h}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\mintinline{c}{ref_iterator}: abstract methods}

  \begin{minted}[fontsize=\small]{c}
int ref_iterator_advance(struct ref_iterator *ref_iterator)
{
	return ref_iterator->vtable->advance(ref_iterator);
}

int ref_iterator_abort(struct ref_iterator *ref_iterator)
{
	return ref_iterator->vtable->abort(ref_iterator);
}
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{refs/iterator.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{The sub-class \mintinline{c}{reflog_iterator}}

  \begin{minted}[fontsize=\scriptsize]{c}
static struct ref_iterator *reflog_iterator_begin(struct ref_store *
                                                  ref_store,
						  const char *gitdir)
{
	struct files_reflog_iterator *iter = xcalloc(1, sizeof(*iter));
	struct ref_iterator *ref_iterator = &iter->base;
	struct strbuf sb = STRBUF_INIT;

	base_ref_iterator_init(ref_iterator,
                               &files_reflog_iterator_vtable, 0);
	strbuf_addf(&sb, "%s/logs", gitdir);
	iter->dir_iterator = dir_iterator_begin(sb.buf);
	iter->ref_store = ref_store;
	strbuf_release(&sb);

	return ref_iterator;
}

  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{refs/files-backend.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Multiple inheritance}

  \begin{minted}[fontsize=\scriptsize]{c}
static int files_reflog_iterator_advance(struct ref_iterator *ref_iterator)
{
	struct files_reflog_iterator *iter =
		(struct files_reflog_iterator *)ref_iterator;
	struct dir_iterator *diter = iter->dir_iterator;
	int ok;

	while ((ok = dir_iterator_advance(diter)) == ITER_OK) {
		int flags;
        ...
}
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{refs/files-backend.c}
  \end{itemize}
\end{frame}

\section{``Metaprogramming'' in Git}

\begin{frame}[fragile]
  \frametitle{Metaprogramming}

  \begin{minted}[fontsize=\small]{c}
#include "cache.h"
...

define_commit_slab(blame_suspects, struct blame_origin *);
static struct blame_suspects blame_suspects;
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{blame.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Metaprogramming}

  \begin{minted}[fontsize=\small]{c}
#define define_commit_slab(slabname, elemtype)   \
	declare_commit_slab(slabname, elemtype); \
	implement_static_commit_slab(slabname, elemtype)
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{commit-slab.h}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Metaprogramming}

  \begin{minted}[fontsize=\small]{c}
#define declare_commit_slab(slabname, elemtype) \
                                                \
struct slabname {                               \
	unsigned slab_size;                     \
	unsigned stride;                        \
	unsigned slab_count;                    \
	elemtype **slab;                        \
}
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{commit-slab-decl.h}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Metaprogramming}

  \begin{minted}[fontsize=\scriptsize,tabsize=8,obeytabs]{c}
#define implement_static_commit_slab(slabname, elemtype) \
	implement_commit_slab(slabname, elemtype, MAYBE_UNUSED static)

#define implement_commit_slab(slabname, elemtype, scope)       \
                                                               \
scope void init_ ##slabname## _with_stride(struct slabname *s, \
                                           unsigned stride)    \
{                                                              \
	unsigned int elem_size;                                \
	if (!stride)                                           \
		stride = 1;                                    \
		...                                            \
}                                                              \
...
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{commit-slab-impl.h}
  \end{itemize}
\end{frame}

\section{Questions?}

\begin{frame}[allowframebreaks]
  \frametitle{References}
  \bibliographystyle{plainurl}
  \bibliography{references.bib}
  \nocite{*}
\end{frame}

\end{document}
