%&tex

\documentclass{beamer}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{mathtools}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{thmtools,thm-restate}
\usepackage{amsfonts}
\usepackage{hyperref}
\usepackage[singlelinecheck=false]{caption}
\usepackage[backend=biber,url=true,doi=true,eprint=false,style=authoryear]{biblatex}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{listings}
\usepackage{subcaption}
\usepackage{graphicx}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\Val}{\text{Val}}
\DeclareMathOperator*{\Ch}{\text{Ch}}
\DeclareMathOperator*{\Pa}{\text{Pa}}
\DeclareMathOperator*{\Sc}{\text{Sc}}
\newcommand{\ov}{\overline}
\newcommand{\tsup}{\textsuperscript}

\newcommand{\set}[1]{\mathbf{#1}}
\newcommand{\pr}{\text{P}}
\newcommand{\eps}{\varepsilon}
\newcommand{\ddspn}[2]{\frac{\partial#1}{\partial#2}}
\newcommand{\iddspn}[2]{\partial#1/\partial#2}
\newcommand{\indep}{\perp}
\renewcommand{\implies}{\Rightarrow}

\newcommand{\bigo}{\mathcal{O}}

\setbeamertemplate{theorems}[plain]

\newcommand{\code}[1]{\lstinline[mathescape=true]{#1}}
\newcommand{\mcode}[1]{\lstinline[mathescape]!#1!}

\title{GoSPN\\\medskip\normalsize A free, open-source library for inference and learning of
  Sum-Product Networks}
\subtitle{FLUSP: FLOSS @ USP}
\date{}
\author{Renato Lui Geh}

\newcommand{\titlelogo}{gospnpher.png}

\addbibresource{references.bib}
\usetheme{macaw}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{Background}

  Let $P(X_1,\ldots,X_k)$ be some high-dimensional distribution.\\\vfill

  \begin{description}
    \item[Objective:] Learn $P$ from some data $D$
    \item[How:] Adjust some parameters $\theta$ such that $P(X_1,\ldots,X_k|\theta)$ is
      maximal
  \end{description}\vfill

  \textbf{In fancy words:} maximize the likelihood of $M$ with respect to data $D$\\\vfill

  But working with full joint distributions is intractable.\\\vfill

  \textbf{Solution:} probabilistic graphical models
\end{frame}

\begin{frame}
  \frametitle{Probabilistic Graphical Models (PGMs)}

  \textbf{Approach:} use graphs to convey interactions between variables (e.g.\ [in]dependencies,
  causality, similarities, etc.)\\\vfill

  \textbf{Examples:} Bayesian Networks, Markov Networks, Hidden Markov Models, Influence Diagrams,
  Credal Networks\\\vfill

  \textbf{More info:} MAC0425, MAC6916\footnote{\url{https://www.ime.usp.br/~ddm/mac6916/}},
  \cite{pgm-koller}, \cite{bayes-net-darwiche}, \cite{pearl-1988}
\end{frame}

\begin{frame}
  \frametitle{Perks and Flaws of PGMs}

  \begin{description}
    \item[Perks]
      \begin{itemize}
        \item Easy to model
        \item Intuitive
        \item Flexible
        \item Results from Graph Theory translate well to PGMs
      \end{itemize}
    \item[Flaws]
      \begin{itemize}
        \item Limited representability
        \item Exact inference is exponential worst case
        \item Learning relies on inference, therefore learning is difficult
        \item Deep PGMs tend to be extremely challenging
      \end{itemize}
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Alternative to ``classical PGMs''}

  \textbf{Sum-Product Networks: A New Deep Architecture (\cite{poon-domingos})}\\\vfill

  \begin{itemize}
    \item Linear exact inference
    \item Deep SPNs are more expressive (\cite{shallow-vs-deep})
    \item More general than other tractable models
    \item Incredible results in various applications\footnote{Image classification and completion,
        natural language modelling, signal and speech, sequence labeling, protein folding, activity
        recognition, ontology matching, autonomous driving of mobile robots, autonomous flight of
        MAVs, OCT segmentation and bacterial examination.}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Recommended reading}

  \textbf{Probabilistic Modelling and Reasoning under Uncertainty}\bigskip

  \begin{description}
    \item[\cite{aima}:] Section 13.1.
    \item[\cite{bayes-net-darwiche}:] Sections 1.1, 1.2, 1.3, 1.4 and 1.5.
    \item[\cite{pgm-koller}:] Sections 1.1 and 1.2.
  \end{description}

\end{frame}

\section{Sum-product networks}

\begin{frame}
  \frametitle{Definition}

  \begin{definition}[\cite{gens-domingos}]~\\
    A sum-product network (SPN) is a DAG where each node can be defined recursively as follows.
    \begin{enumerate}
      \item A tractable univariate probability distribution is an SPN\@.
      \item A product of SPNs with disjoint scopes is an SPN\@.
      \item A weighted sum of SPNs with the same scope is an SPN, provided all weights are positive.
      \item Nothing else is an SPN\@.
    \end{enumerate}
  \end{definition}
\end{frame}

\begin{frame}
  \frametitle{Example of SPN}

  \begin{figure}
    \centering
    \includegraphics[height=0.5\textheight]{imgs/sample_spn.png}
  \end{figure}

  Where $[X_i=j]$ is an indicator variable where $[X_i=j]=0$ if $X_i\neq j$, and $1$ otherwise.
\end{frame}

\begin{frame}
  \frametitle{Properties I}

  Let $\mathbf{X}=\{X_1,\ldots,X_n\}$ be the scope of all variables. The scope of an internal node
  is the union of its children scope. The scope of a leaf is the scope of its distribution.\\\vfill

  If an SPN $S$ is valid, then $S(\mathbf{X})=P(\mathbf{X})$ (\cite{poon-domingos}).\\\vfill

  \begin{definition}[Completeness]
    An SPN $S$ is complete iff all children of the same sum node have the same scope.
  \end{definition}
\end{frame}

\begin{frame}
  \frametitle{Properties II}

  \begin{definition}[Consistency]
    An SPN $S$ is consistent iff no variable appears with a value $v$ in one child of a product
    node, and valued $u$, with $u\neq v$, in another.
  \end{definition}\vfill

  \begin{theorem}[\cite{poon-domingos}]
    An SPN $S$ is valid if $S$ is complete and consistent.
  \end{theorem}\vfill

  Learning consistent SPNs is tricky. We instead try to learn decomposable SPNs.
\end{frame}

\begin{frame}
  \frametitle{Properties III}

  \begin{definition}[Decomposability]
    An SPN $S$ is decomposable iff no variable appears in more than one child of a product node.
  \end{definition}\vfill

  \textbf{Intuition for completeness and decomposability:}\medskip
  \begin{description}
    \item[Sums:] children of sums group up ``similar'' variables
    \item[Products:] children of products are ``independent'' of siblings
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Inference I}

  Let $\mathbf{X}=\{X_1,X_2\}$. And let $\mathbf{x}=\{X_1=0\}$ (note that $X_2$ is missing in
  valuation). We want to find $P(\mathbf{X}=\mathbf{x})$. How?\vfill

  \begin{figure}
    \centering\includegraphics[height=0.55\textheight]{imgs/sample_spn_prob.png}
  \end{figure}\vfill

  ``Bottom-up'' evaluation computing leaves first, going ``up'' until root is reached.
  $P(\mathbf{x})=S(\mathbf{x})=\text{value of root given }\mathbf{x}=0.31$.
\end{frame}

\begin{frame}
  \frametitle{Inference II}

  \textbf{Maximum a posteriori hypothesis (MAP):} find

  \begin{equation*}
    \max_{\mathbf{x}} P(\mathbf{X}=\mathbf{x})
  \end{equation*}\vfill

  \textbf{Problem:} MAP is NP-hard (\cite{theoretical-spn,cmc2017,mei2018}).\vfill

  \textbf{Solution:} Approximate algorithms.\vfill

  \begin{itemize}
    \item Max-Product Algorithm (\cite{poon-domingos})
    \item Argmax-Product Algorithm (\cite{cmc2017})
    \item Exact solver with heuristics (\cite{mei2018})
    \item K-Best Tree (\cite{mei2018})
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Inference III}

  \textbf{Max-Product Algorithm:} replace sums with max nodes. Compute probability of evidence.

  \begin{figure}
    \centering\includegraphics[height=0.55\textheight]{imgs/sample_mpn_prob.png}
  \end{figure}

  \textbf{Most probable explanation:} after bottom-up evaluation, find maximal tree in a top-down
  fashion.
\end{frame}

\begin{frame}
  \frametitle{Recommended reading}

  \textbf{Sum-Product Networks}\bigskip

  \begin{description}
    \item[\cite{poon-domingos}:] Sections 1 and 2.
    \item[\cite{gens-domingos}:] Sections 1 and 2.
    \item[\cite{geh-tcc}\footnote{Available at
      \url{https://www.ime.usp.br/~renatolg/mac0499/docs/thesis.pdf}}:] Chapter 2.
    \item[\cite{peharz-spn}:] Sections 4.0, 4.1, 4.2.0, 4.2.1, 4.3.0 and 4.3.1.
  \end{description}

\end{frame}

\section{GoSPN}

\begin{frame}
  \frametitle{What is GoSPN?}

  \begin{itemize}
    \item Inference
      \begin{itemize}
        \item Probability of evidence
        \item MAP
        \item MPE
      \end{itemize}
    \item Learning
      \begin{itemize}
        \item \cite{gens-domingos}
        \item \cite{clustering})
      \end{itemize}
    \item Utility functions
      \begin{itemize}
        \item Depth of model
        \item Compute scope
        \item Easy CPU concurrency
      \end{itemize}
    \item Real world applications
      \begin{itemize}
        \item Classification
        \item Image reconstruction
      \end{itemize}
    \item Must be \textbf{fast and efficient}, but also easy to use and have \textbf{readable code}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Why Go?}

  \begin{itemize}
    \item Strong and static typed
    \item Fast garbage collection
    \item Directly compiled to machine code
    \item Fast compilation times
    \item Cross-platform compilation to a single binary
    \item Built-in testing and profiling framework
    \item Static code analysis guarantees consistency
    \item Native CPU concurrency support
    \item Minimalistic
    \item Easy to learn like Python, but fast and efficient like C
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Code structure}

  \begin{description}
    \item[app] - applications in real-world use cases
    \item[common] - data structures and functions commonly used
    \item[conc] - concurrency data structures and algorithms
    \item[data] - dataset manipulation, importing and exporting
    \item[io] - input/output for dataset, images and SPNs
    \item[learn] - SPN derivation and learning algorithms
    \item[score] - scoring
    \item[spn] - inference, topology, searching on SPNs
    \item[sys] - GC, logging, randomization and time
    \item[utils] - auxiliary algorithms used in learning
  \end{description}
\end{frame}

\begin{frame}
  \frametitle{Documentation and guidelines}

  \textbf{Repository:}

  \url{https://github.com/RenatoGeh/gospn}\vfill

  \textbf{Documentation:}

  \url{https://godoc.org/github.com/RenatoGeh/gospn}\vfill

  \textbf{Contribution Guidelines:}

  \footnotesize\url{https://github.com/RenatoGeh/gospn/blob/dev/CONTRIBUTING.md}
\end{frame}

\begin{frame}
  \frametitle{Next steps}

  \begin{figure}
    \centering\includegraphics[height=0.3\textheight]{imgs/peharz_et_al.png}
  \end{figure}

  \begin{center}
    Implement \cite{deep-learn-spn} through TensorFlow.
  \end{center}\vfill

  \textbf{Problem:} Go has no full TensorFlow API.\vfill

  \textbf{Possible solutions:}
  \begin{enumerate}
    \item \href{https://github.com/gorgonia/gorgonia}{Gorgonia}
      (\url{https://github.com/gorgonia/gorgonia})
    \item Modelling in Python, training and inference in Go
    \item Complete port of GoSPN to Python
  \end{enumerate}
\end{frame}

\begin{frame}[t,allowframebreaks]
  \frametitle{References}
  \printbibliography[heading=none]
\end{frame}

\end{document}
