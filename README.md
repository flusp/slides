# Slides

This is a collection of all FLUSP LaTeX slides.

It is highly recommended to use the
[Macaw](https://gitlab.com/flusp/macaw) beamer LaTeX template for
slides.
